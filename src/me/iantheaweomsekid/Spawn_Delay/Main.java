package me.iantheaweomsekid.Spawn_Delay;


import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;


public class Main extends JavaPlugin {
    public final Logger logger = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        logger.info("[Spawn Delay] has been enabled");}

    @Override
    public void onDisable() {
        logger.info("[Spawn Delay] has been disabled");}

    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        Player player = (Player) sender;
        if (commandLabel.equalsIgnoreCase("out"))  {
            Location originalLocation = player.getLocation();
            logger.info("[Spawn Delay] original location: " + originalLocation.toString());
            player.sendMessage(ChatColor.GOLD + "Teleportation will commence in 5 seconds. Don't move");
            player.sendMessage(ChatColor.GOLD + "5");
            sleepOneSecond(player, originalLocation);
            player.sendMessage(ChatColor.GOLD + "4");
            sleepOneSecond(player, originalLocation);
            player.sendMessage(ChatColor.GOLD + "3");
            sleepOneSecond(player, originalLocation);
            player.sendMessage(ChatColor.GOLD + "2");
            sleepOneSecond(player, originalLocation);
            player.sendMessage(ChatColor.GOLD + "1");
            player.teleport(player.getWorld().getSpawnLocation());
            player.sendMessage(ChatColor.GOLD + "You have been successfully teleported to spawn");
            player.sendMessage(ChatColor.GOLD + "Have a nice day :)");
            return true;

        }

        return false;
    }

    private void sleepOneSecond(Player p, Location l) {
        try {
            Thread.sleep(1000L);
            // do NOT do originalLocation != player.getLocation()  -- why?
            // because that will do a REFERENCE comparison, and we want to compare
            // location *values* so we use .equals() instead
            p = Bukkit.getPlayer(p.getName());
            Location currentLocation = p.getLocation();
            Vector velocity = p.getVelocity();
            logger.info("[Spawn Delay] current location: " + currentLocation.toString());
            logger.info("[Spawn Delay] current velocity: " + velocity.length());
            if (velocity.length() > 0.1 || !l.equals(currentLocation)) {
                // player moved, BAIL OUT!!!!!!!
                p.sendMessage(ChatColor.RED + "Telepotation cancelled. You moved!!!");
                throw new RuntimeException("player location changed!!!");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e); // stupid checked exception on sleep
        }

    }

}




